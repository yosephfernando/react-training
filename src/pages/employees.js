import React, { Component } from 'react';
import { Table, Pagination, PaginationItem, PaginationLink, Button } from 'reactstrap';
import { withRouter } from 'react-router-dom';

class Employees extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            isLoading: false,
            currentPage: 1,
            employeesPerPage: 20
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }

    handleClick(event) {
        this.setState({
            currentPage: Number(event.target.id)
        });
    }

    handleDelete(event) {
        fetch('http://dummy.restapiexample.com/api/v1/delete/'+event.target.id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(datas =>
            fetch('http://dummy.restapiexample.com/api/v1/employees')
              .then(response => response.json())
              .then(data =>
                this.setState({ employees: data, isLoading: false })
               )
        ).catch(err => err);
    }

    handleUpdate(event){
        const url = "/update-employee/"+event.target.id
        this.props.history.push(url);
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        fetch('http://dummy.restapiexample.com/api/v1/employees')
          .then(response => response.json())
          .then(data =>
            this.setState({ employees: data, isLoading: false })
          );
    }
    
    render(){
        const { employees, isLoading, currentPage, employeesPerPage } = this.state;

        const indexOfLastEmployee = currentPage * employeesPerPage;
        const indexOfFirstEmployee = indexOfLastEmployee - employeesPerPage;
        const currentEmployee = employees.slice(indexOfFirstEmployee, indexOfLastEmployee);

        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(employees.length / employeesPerPage); i++) {
            pageNumbers.push(i);
        }

        const renderPageNumbers = pageNumbers.map(number => {
            return (
                <PaginationItem>
                    <PaginationLink key={number} id={number} onClick={this.handleClick}>
                        {number}
                    </PaginationLink>
                </PaginationItem>
            );
        });

        if (isLoading) {
            return <p className="text-center">Loading ...</p>;
        } else {
            return (
                <div className="container-fluid">
                    <div className="d-flex mt-3">
                      <Table bordered>
                          <thead>
                              <tr>
                                  <th>Employee ID</th>
                                  <th>Name</th>
                                  <th>Salary</th>
                                  <th>Age</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                             {currentEmployee.map(employee =>
                                <tr key={employee.id}>
                                    <th scope="row">{employee.id}</th>
                                    <td>{employee.employee_name}</td>
                                    <td>{employee.employee_salary}</td>
                                    <td>{employee.employee_age}</td>
                                    <td className="text-center">
                                        <Button className="btn-warning mr-1" id={employee.id} onClick={this.handleUpdate}>UPDATE</Button>
                                        <Button className="btn-danger" id={employee.id} onClick={this.handleDelete}>DELETE</Button>
                                    </td>
                                </tr>
                             )}
                          </tbody>
                      </Table>
                    </div>
                    <div className="float-right">
                        <Pagination aria-label="Page navigation example">
                            {renderPageNumbers}
                        </Pagination>
                    </div>
                </div>
              );
        }
    }
  }
  
  export default withRouter(Employees);