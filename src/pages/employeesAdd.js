import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class EmployeesAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            age: null,
            salary: null
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        fetch('http://dummy.restapiexample.com/api/v1/create', {
            method: 'POST',
            body: JSON.stringify(this.state),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => 
            alert('Success Add '+data.name)
        ).catch(err => err);
        event.preventDefault();
    }

    componentDidMount() {}

    render(){
        return (
            <div className="container-fluid">
                <div className="col-6 mx-auto mt-3">
                    <Form onSubmit={this.handleSubmit}>
                        <h3>Employee Form</h3>
                        <FormGroup>
                            <Label for="exampleEmail">Name</Label>
                            <Input type="text" name="name" id="name" value={this.state.name} onChange={this.handleChange} placeholder="Employee name" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleEmail">Salary</Label>
                            <Input type="text" name="salary" id="salary" value={this.state.salary} onChange={this.handleChange} placeholder="ex 1.000.***" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleEmail">Age</Label>
                            <Input type="text" name="age" id="age" value={this.state.age} onChange={this.handleChange} placeholder="Employee age" />
                        </FormGroup>
                        <Button type="submit" className="btn-success float-right">Save</Button>
                    </Form>
                </div>
            </div>
        );
    }
}
  
export default EmployeesAdd;