import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class EmployeesUpdate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            employees: {},
            employeeSave: {}
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const employeesSave = this.state.employeeSave;
        const employees = this.state.employees;
        
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        employees[name] = value

        if(name == "employee_name"){
            employeesSave["name"] = value;
        }else if(name == "employee_salary"){
            employeesSave["salary"] = value;
        }else{
            employeesSave["age"] = value;
        }        

        this.setState({
            employees: employees,
            employeeSave: employeesSave
        });

        console.log("employeesSave",  this.state.employeeSave)
    }

    handleSubmit(event) {
        event.preventDefault();

        if(this.state.employeeSave.name == undefined){
            this.state.employeeSave.name = this.state.employees.employee_name;
        }
        
        if(this.state.employeeSave.salary == undefined){
            this.state.employeeSave.salary = this.state.employees.employee_salary;
        }
        
        if(this.state.employeeSave.age == undefined){
            this.state.employeeSave.age = this.state.employees.employee_age;
        }

        console.log("employeesSave Simb",  this.state.employeeSave)

        fetch('http://dummy.restapiexample.com/api/v1/update/'+this.state.employees.id, {
            method: 'PUT',
            body: JSON.stringify(this.state.employeeSave),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => 
            alert('Success Update '+data.name)
        ).catch(err => err);
    }

    componentDidMount() {
        const { match: { params } } = this.props;
        this.setState({ isLoading: true });
        fetch('http://dummy.restapiexample.com/api/v1/employee/'+params.employeeId)
          .then(response => response.json())
          .then(datas =>
            this.setState({ employees: datas, isLoading: false })
          );
    }

    render(){
        const { employees, isLoading } = this.state;
        if (isLoading) {
            return <p className="text-center">Loading ...</p>;
        } else {
            return (
                <div className="container-fluid">
                    <div className="col-6 mx-auto mt-3">
                        <Form onSubmit={this.handleSubmit}>
                            <h3>Employee Update Form</h3>
                            <FormGroup>
                                <Label for="exampleEmail">Name</Label>
                                <Input type="text" name="employee_name" id="employee_name" value={employees.employee_name} onChange={this.handleChange} placeholder="Employee name" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">Salary</Label>
                                <Input type="text" name="employee_salary" id="employee_salary" value={employees.employee_salary} onChange={this.handleChange} placeholder="ex 1.000.***" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">Age</Label>
                                <Input type="text" name="employee_age" id="employee_age" value={employees.employee_age} onChange={this.handleChange} placeholder="Employee age" />
                            </FormGroup>
                            <Button type="submit" className="btn-success float-right">Save</Button>
                        </Form>
                    </div>
                </div>
            );
        }
    }
}
  
export default EmployeesUpdate;