import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Table
} from 'reactstrap';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Employees from '../src/pages/employees';
import EmployeesAdd from '../src/pages/employeesAdd';
import EmployeesUpdate from '../src/pages/employeesUpdate';

const App = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  return (
    <Router>
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">Employee</NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink>
                  <Link to="/employees">Employee Datas</Link>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink>
                  <Link to="/add-employee">Add Employee</Link>
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        <Switch>
          <Route path="/employees">
            <Employees />
          </Route>
          <Route path="/add-employee">
            <EmployeesAdd />
          </Route>
          <Route path="/update-employee/:employeeId" component={EmployeesUpdate}></Route>
          <Route path="/">
            <Employees />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;